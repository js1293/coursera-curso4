var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');


describe('Testing Bicicletas', function() {
    beforeEach( function(done){
        var mongoDb = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDb, {useNewUrlParser:true, poolSize: 10});
        
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDb connection error: '));
        db.once('open', function(){
            console.log('conectado a la bd mongo');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(error, success){
            if(error) console.log(error);
            mongoose.disconnect(error); 
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('Crea una instancia de bicicleta', () =>{
            var bici = Bicicleta.createInstance(1, "negra", "urbana", [6.2545214,-75.6077224]);
            
            expect(bici.code).toBe(1);
            expect(bici.color).toBe('negra');
            expect(bici.modelo).toBe('urbana');
            expect(bici.ubicacion[0]).toBe(6.2545214);
            expect(bici.ubicacion[1]).toBe(-75.6077224);
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('Comienza vacio', (done) =>{
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                done();
            });
        })
    });

    describe('Bicicleta.add', () => {
        it('Agregamos una bicicleta', (done) =>{
            var biciUno = new Bicicleta({code:1, color:'Azul', modelo:'Urbana'});

            Bicicleta.add(biciUno, function(err, newBici){
                if(err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(biciUno.code);
                    done();
                });
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('Debe devolver bici con code 1', (done) =>{
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);

                var biciUno = new Bicicleta({code:1, color:'Azul', modelo:'Urbana'});
                Bicicleta.add(biciUno, function(err, newBici){
                    if(err) console.log(err);

                    var biciDos = new Bicicleta({code:2, color:'Roja', modelo:'Urbana'});
                    Bicicleta.add(biciDos, function(err, newBici2){
                        if(err) console.log(err);

                        Bicicleta.findByCode(1, function(error, targetBici){
                            console.log(targetBici);
                            expect(targetBici.code).toBe(biciUno.code);
                            expect(targetBici.color).toBe(biciUno.color);
                            expect(targetBici.modelo).toBe(biciUno.modelo);

                            done();
                        });
                    });
                });
            });
        });
    });

    describe('Bicicleta.removeByCode', () => {
        it('Se debe remover la bici con code 1', (done) =>{
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);

                var biciUno = new Bicicleta({code:1, color:'Azul', modelo:'Urbana'});
                Bicicleta.add(biciUno, function(err, newBici){
                    if(err) console.log(err);

                    var biciDos = new Bicicleta({code:2, color:'Roja', modelo:'Urbana'});
                    Bicicleta.add(biciDos, function(err, newBici2){
                        if(err) console.log(err);

                        Bicicleta.removeByCode(1, function(error, targetBici){
                            console.log(targetBici);

                            Bicicleta.allBicis(function(err, bicis2){
                                expect(bicis2.length).toBe(1);
                                expect(bicis2[0].code).toBe(biciDos.code);
                                expect(bicis2[0].color).toBe(biciDos.color);
                                expect(bicis2[0].modelo).toBe(biciDos.modelo);
                                done();
                            });
                        });
                    });
                });
            });
        });
    });

});

// beforeEach(()=>{
//     Bicicleta.allBicis = [];
// });

// describe('Bicicleta.allBicis', () => {
//     it('Comienza vacio', () =>{
//         expect(Bicicleta.allBicis.length).toBe(0);
//     })
// });


// describe('Bicicleta.add', () => {
//     it('Agregamos una', () =>{
//         expect(Bicicleta.allBicis.length).toBe(0);

//         var biciUno = new Bicicleta(1, 'Azul', 'Urbana', [6.2545214,-75.6077224]);
//         Bicicleta.add(biciUno);

//         expect(Bicicleta.allBicis.length).toBe(1);
//         expect(Bicicleta.allBicis[0]).toBe(biciUno);
//     })
// });

// describe('Bicicleta.findById', () => {
//     it('Debe devolver bici con id 1', () =>{
//         expect(Bicicleta.allBicis.length).toBe(0);

//         var biciUno = new Bicicleta(1, 'Azul', 'Urbana', [6.2545214,-75.6077224]);
//         var biciDos = new Bicicleta(2, 'Negra', 'Urbana', [6.2547861,-75.5946455])

//         Bicicleta.add(biciUno);
//         Bicicleta.add(biciDos);

//         var targetBici = Bicicleta.findById(1);

//         expect(targetBici.id).toBe(1);
//         expect(targetBici.color).toBe(biciUno.color);
//         expect(targetBici.modelo).toBe(biciUno.modelo);
        
//     })
// });

// describe('Bicicleta.removeById', () => {
//     it('Se debe remover la bici con id 1', () =>{
//         expect(Bicicleta.allBicis.length).toBe(0);

//         var biciUno = new Bicicleta(1, 'Azul', 'Urbana', [6.2545214,-75.6077224]);
//         var biciDos = new Bicicleta(2, 'Negra', 'Urbana', [6.2547861,-75.5946455])

//         Bicicleta.add(biciUno);
//         Bicicleta.add(biciDos);

//         Bicicleta.removeById(1);

//         expect(Bicicleta.allBicis.length).toBe(1);
//         expect(Bicicleta.allBicis[0].id).toBe(2);
        
        
//     })
// });

