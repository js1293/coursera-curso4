var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var Usuario = require('../../models/usuario');
var Reserva = require('../../models/reserva');
const { usuarios_list } = require('../../controllers/api/usuarioControllerAPI');

describe('Testing Usuarios', function() {
    beforeEach( function(done){
        var mongoDb = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDb, {useNewUrlParser:true, poolSize: 10});
        
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDb connection error: '));
        db.once('open', function(){
            console.log('conectado a la bd mongo');
            done();
        });
    });

    afterEach(function(done){
        Reserva.deleteMany({}, function(error, success){
            if(error) console.log(error);
            Usuario.deleteMany({}, function(error, success){
                if(error) console.log(error);
                Bicicleta.deleteMany({}, function(error, success){
                    if(error) console.log(error);
                    done();
                });
            });
        });
    });

    describe('Cuando un usuario reserva una bici', () => {
        it('Debe existir la reserva en la bd mongo', (done) =>{
            const usuario = new Usuario({nombre: 'Johan'});
            usuario.save();

            var bicicleta = new Bicicleta({code:1, color:'Azul', modelo:'Urbana'});
            bicicleta.save();

            var hoy = new Date();
            var tomorrow = new Date();
            tomorrow.setDate(hoy.getDate() + 1);
            usuario.reservar(bicicleta._id, hoy, tomorrow, function(err, reserva){
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas){
                    console.log(reservas[0]);
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                });
            });
        });
    });
});