var mongoose = require('mongoose');
var server = require('../../bin/www');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');

var base_url = "http://localhost:3000/api/bicicletas";


mongoose.disconnect(''); //Desconectar mongose de server
describe('Bicicletas API', function () {
    beforeEach( function(done){
        
        console.log('testeando...')
        var mongoDb = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDb, {useNewUrlParser:true, poolSize: 10});
        
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDb11 connection error: '));
        db.once('open', function(){
            console.log('conectado a la bd mongo');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(error, success){
            if(error) console.log('oe:' + error);
            mongoose.disconnect(error); 
            done();
        });
    });

    describe('GET bicicletas /', () => {
        it('Status 200', (done) => {
            request.get(base_url, function(error, response, body){
                res = JSON.parse(body);
                console.log(res.bicicletas);
                expect(response.statusCode).toBe(200);
                expect(res.bicicletas.length).toBe(0);
                done();
            });
        });
    });

    describe('POST bicicletas /create', () => {
        it('Status 200', (done) => {
            var headers = {'content-type': 'application/json'};
            var biciUno = Bicicleta.createInstance(1, 'Azul', 'Urbana', [6.2545214,-75.6077224]);
            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: JSON.stringify(biciUno)
            }, function(error,response, body){
                expect(response.statusCode).toBe(200);
                Bicicleta.findByCode(1, function(err, biciCreated){
                    expect(biciCreated.color).toBe('Azul');
                    done();
                });
            });
            
        });
    });

    describe('POST bicicletas /update', () => {
        it('Status 200', (done) => {
            var headers = {'content-type': 'application/json'};
            var biciUno = Bicicleta.createInstance(1, 'Azul', 'Urbana', [6.2545214,-75.6077224]);
            Bicicleta.add(biciUno, function(err, resp){
                biciUno.color = 'Rojo';

                request.post({
                    headers: headers,
                    url: 'http://localhost:3000/api/bicicletas/update',
                    body: JSON.stringify(biciUno)
                }, function(error,response, body){
                    expect(response.statusCode).toBe(200);
                    Bicicleta.findByCode(1, function(err, biciCreated){
                        expect(biciCreated.color).toBe('Rojo');
                        done();
                    });
                });
            });            
        });
    });

    describe('DELETE bicicletas /delete', () => {
        it('Status 200', (done) => {
            var headers = {'content-type': 'application/json'};
            var biciUno = Bicicleta.createInstance(1, 'Azul', 'Urbana', [6.2545214,-75.6077224]);
            
            Bicicleta.add(biciUno, function(err, resp){
                request.delete({
                    headers: headers,
                    url: 'http://localhost:3000/api/bicicletas/delete',
                    body: JSON.stringify(biciUno)
                }, function(error,response, body){
                    expect(response.statusCode).toBe(204);
    
                    Bicicleta.allBicis(function(err, bicis){
                        expect(bicis.length).toBe(0);
                        done();
                    });
                });
            });
        });
    });
});