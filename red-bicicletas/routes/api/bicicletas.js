var express = require('express');
var router = express.Router();
var bicicletaController = require('../../controllers/api/bicicletaControllerAPI');

router.get('/', bicicletaController.bicicleta_list);
router.post('/create', bicicletaController.bicicleta_create_post);
router.delete('/delete', bicicletaController.bicicleta_delete_post);
router.post('/update', bicicletaController.bicicleta_update_post);

module.exports = router;