const Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req, res){   
    Bicicleta.allBicis(function(err, bicis){
        res.status(200).json({
            bicicletas: bicis
        });
    });
}

exports.bicicleta_create_post = function(req, res){
    var bici = Bicicleta.createInstance(req.body.code, req.body.color, req.body.modelo, req.body.ubicacion);
    Bicicleta.add(bici, function(err, resp){
        res.status(200).json({
            bicicletas: bici
        });
    });
}

exports.bicicleta_delete_post = function(req, res){
    Bicicleta.removeByCode(req.body.code, function(err, resp){
        res.status(204).send();
    });
}

exports.bicicleta_update_post = function(req, res){
    Bicicleta.findByCode(req.body.code, function(err, bici){
        bici.modelo = req.body.modelo;
        bici.color = req.body.color;
        bici.ubicacion = req.body.ubicacion;
    
        Bicicleta.updateByCode(bici, function(err, bicis){
            res.status(200).json({
                bicicletas: bicis
            });
        });
    });
    
}